$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){	
		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
	
	$.ajax({
		
		url:"http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query="+palabra,
		success: function(respuesta) {
			console.log(respuesta);
			peliculas = $('#miLista'); //DIV donde se cargará la lista de peliculas

			setTimeout(function () {
				$('#loader').remove(); //Se elimina la imagen de "cargando" (los engranes)

				//Para cada elemento en la lista de resultados (para cada pelicula)
				$.each(respuesta.results, function(index, elemento) {
					//La función crearMovieCard regresa el HTML con la estructura de la pelicula
					cardHTML = crearMovieCard(elemento); 
					peliculas.append(cardHTML);
				});

			}, 3000); //Tarda 3 segundos en ejecutar la función de callback
			          //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.
		},
		error: function() {
			console.log("No se ha podido obtener la información");
			$('#loader').remove();
			$('#miLista').html('No se ha podido obtener la información');
		},

		beforeSend: function() { 
			//ANTES de hacer la petición se muestra la imagen de cargando.
			console.log('CARGANDO');
			$('#miLista').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
		},
	});	
});
});



function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
	var cardHTML =
	'<!-- CARD -->'
	+'<div class="col-md-4">'
		+'<div class="card">'
		   +'<div class="card-header">'
			  +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		   +'</div>'
		   +'<div class="card-body">'
			  +'<h2 class="card-title">'+movie.original_title+'</h2>'
			  +'<p>'+movie.release_date+'</p>'
		   +'</div>'
		+'</div>'
	+'</div>'
	+'<!-- CARD -->';

		return cardHTML;
}



